import { NG2SYNPage } from './app.po';

describe('ng2-syn App', function() {
  let page: NG2SYNPage;

  beforeEach(() => {
    page = new NG2SYNPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
