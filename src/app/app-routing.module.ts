import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavComponent }   from './nav/nav.component';
import { DropdownComponent }   from './dropdown/dropdown.component';
import { BreadcrumbComponent }   from './breadcrumb/breadcrumb.component';
import { TabsComponent }   from './tabs/tabs.component';
import { TooltipComponent }   from './tooltip/tooltip.component';
import {ModalComponent} from "./modal/modal.component";
import {TabularComponent} from "./tabular/tabular.component";

const routes: Routes = [
    { path: '', redirectTo: '/nav', pathMatch: 'full' },
    { path: 'nav',  component: NavComponent },
    { path: 'breadcrumb',  component: BreadcrumbComponent },
    { path: 'dropdowns',  component: DropdownComponent },
    { path: 'tabs',  component: TabsComponent },
    { path: 'tooltips',  component: TooltipComponent },
    { path: 'modals',  component: ModalComponent },
    { path: 'tabular', component: TabularComponent}
];
@NgModule({
    imports: [ RouterModule.forRoot(routes,{ useHash: true }) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
