import {Component, ViewChild, ElementRef} from '@angular/core';
import {INav,INavItem,NavDirection,NavTheme} from "../../modules/nav/nav.interfaces";
import {AppService} from "../app.service";
import {BaseComponent} from "../base.component";

@Component({
    selector: 'nav-component',
    templateUrl: 'nav.tpl.html',
    styles: [':host { display:flex; flex: 1; min-width: 0; }']
})
export class NavComponent extends BaseComponent {

    private navPlainOptions: INavItem[] = [
        {
            label: "Documentation",
            path: "/documentation"
        },
        {
            label: "Support",
            path: "/support"
        },
        {
            label: "Version 1.0.0",
            path: "/version"
        }
    ];

    private navIconOptions: INavItem[] = [
        {
            label: "Documentation",
            path: "/documentation",
            icon:"insert_drive_file"
        },
        {
            label: "Support",
            path: "/support",
            icon:"live_help"
        },
        {
            label: "Version 1.0.0",
            path: "/version",
            icon:"info_outline",
            active:true
        }
    ];

    private navGroupOptions: INavItem[] = [
        {
          label: 'heading 1'
        },
        {
            label: "Documentation",
            path: "/documentation",
            icon:"insert_drive_file"
        },
        {
            label: 'heading 2'
        },
        {
            label: "Support",
            path: "/support",
            icon:"live_help"
        },
        {
            label: "Version 1.0.0",
            path: "/version",
            icon:"info_outline"
        }
    ];

    private examplePlainNav:INav = {
        config:{
            direction:NavDirection.Horizontal
        },
        items: this.navPlainOptions
    };

    private examplePlainVerticalNav:INav = {
        config:{
            direction:NavDirection.Vertical
        },
        items: this.navPlainOptions
    };

    private exampleIconVerticalNav:INav = {
        config:{
            direction:NavDirection.Vertical
        },
        items: this.navIconOptions
    };

    private exampleGroupVerticalNav:INav = {
        config:{
            direction:NavDirection.Vertical
        },
        items: this.navGroupOptions
    };

    private examplePlainThemeNav:INav = {
        config:{
            direction:NavDirection.Vertical,
            theme:NavTheme.Plain
        },
        items: this.navPlainOptions
    };

    private exampleBlueThemeNav:INav = {
        config:{
            direction:NavDirection.Vertical,
            theme:NavTheme.Blue
        },
        items: this.navPlainOptions
    };

    private contentsNav:INav = {
        config:{
            direction:NavDirection.Vertical,
            theme:NavTheme.Plain
        },
        items: [
            {
            label: 'Contents'
            },
            {
                label: "Usage",
                path:'#usage'
            },
            {
                label: "Examples",
                path:'#examples'
            },
            {
                label: "Code",
                path:'#code'
            },
            {
                label: "API Reference",
                path:'#api'
            }
        ]
    };

    constructor(appService:AppService){
        super(appService);
    }
}
