import {Component, OnInit} from '@angular/core';
import {INav, NavDirection, INavItem} from "../modules/nav/nav.interfaces";
import {AppService} from "./app.service";
import {Router, NavigationStart, NavigationEnd} from "@angular/router";


@Component({
  selector: 'app-component',
  templateUrl: 'app.component.tpl.html',
})
export class AppComponent implements OnInit {

  protected hasActiveRouteBeenChecked:boolean = false;

  private mainNav:INav = {
    config:{
      direction:NavDirection.Horizontal
    },
    items: [
      {
        label: "Documentation",
        path: "#",
        items:[
          {
            label: "SYN",
            path: "/SYN/doc/index.html"
          },
          {
            label: "NG2-SYN",
            path: "/",
            active:true
          }
        ]
      },
      {
        label: "Version 1.0.0",
        path: "/"
      }
    ]
  };

  private sidebarNav:INav = {
    config:{
      direction:NavDirection.Vertical
    },
    items: [
      {
        label: "Components"
      },
      {
        label: "Nav",
        path: "/nav",
        icon: "menu"
      },
      {
        label: "Breadcrumb",
        path: "/breadcrumb",
        icon: "label_outline"
      },
      {
        label: "Dropdowns",
        path: "/dropdowns",
        icon: "arrow_drop_down_circle"
      },
      {
        label: "Tabs",
        path: "/tabs",
        icon: "tab"
      },
      {
        label: "Tooltips",
        path: "/tooltips",
        icon: "mode_comment"
      },
      {
        label: "Modals",
        path: "/modals",
        icon: "aspect_ratio"
      },
      {
        label: "Tabular",
        path: "/tabular",
        icon: "border_all"
      }
    ]
  };

  private gotoRoute = (item:INavItem) =>{
    this.appService.gotoRoute(item.path);
    item.active = true; //set item to active
  };

  private checkActiveNavItem():void{
    let activeItem = this.sidebarNav.items.filter(item => {
         return (item.path) ? this.appService.isRouteActive(item.path) : false;
    });
    if(activeItem.length)
      activeItem[0].active = true; //set item to active
  }

  constructor(private appService:AppService,
              private router:Router){}

  ngOnInit(){
    this.router.events
        .subscribe((event) => {
          if (event instanceof NavigationEnd && !this.hasActiveRouteBeenChecked) {
            this.hasActiveRouteBeenChecked = true;
            this.checkActiveNavItem();
          }
        });
  }
}
