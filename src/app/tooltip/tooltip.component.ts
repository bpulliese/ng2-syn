import {Component} from '@angular/core';
import {INav,NavDirection,NavTheme} from "../../modules/nav/nav.interfaces";
import {AppService} from "../app.service";
import {BaseComponent} from "../base.component";

@Component({
    selector: 'tooltip-component',
    templateUrl: 'tooltip.tpl.html',
    styles: [':host { display:flex; flex: 1; min-width: 0; }']
})
export class TooltipComponent extends BaseComponent {

    private dynamicTooltipText:string = "sample text";
    private contentsNav:INav = {
        config:{
            direction:NavDirection.Vertical,
            theme:NavTheme.Plain
        },
        items: [
            {
                label: 'Contents'
            },
            {
                label: "Usage",
                path:'#usage'
            },
            {
                label: "Examples",
                path:'#examples'
            },
            {
                label: "Code",
                path:'#code'
            },
            {
                label: "API Reference",
                path:'#api'
            }
        ]
    };

    constructor(appService:AppService){
        super(appService);
    }
}
