import {Injectable, Inject} from '@angular/core';
import {PageScrollInstance, PageScrollService, PageScrollingViews} from 'ng2-page-scroll/ng2-page-scroll';
import {Router} from "@angular/router";
import {DOCUMENT} from "@angular/platform-browser";

@Injectable()
export class AppService {

    constructor(private router:Router,
                private pageScrollService: PageScrollService,
                @Inject(DOCUMENT) private document: any){}

    public scrollTo = (scrollToId:string, scrollableEl:PageScrollingViews) =>{
        let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInlineInstance(this.document, scrollToId, scrollableEl);
        this.pageScrollService.start(pageScrollInstance);
    };

    public gotoRoute = (path:string) =>{
        this.router.navigateByUrl(path);
    };

    public isRouteActive(path:string):boolean{
        return  (this.router.url == path);
    }

}