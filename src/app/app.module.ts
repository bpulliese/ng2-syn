import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";
import { AppComponent }  from './app.component';
import { HighlightJsModule, HighlightJsService } from 'angular2-highlight-js';
import {Ng2PageScrollModule} from 'ng2-page-scroll/ng2-page-scroll';
import {Ng2SynModule} from "../modules/index";
import { AppRoutingModule }  from './app-routing.module';
import {NavComponent} from "./nav/nav.component";
import {AppService} from "./app.service";
import {DropdownComponent} from "./dropdown/dropdown.component";
import {BreadcrumbComponent} from "./breadcrumb/breadcrumb.component";
import {TabsComponent} from "./tabs/tabs.component";
import {TooltipComponent} from "./tooltip/tooltip.component";
import {ModalComponent} from "./modal/modal.component";
import {MyCustomModalComponent} from "./modal/custom-modal.component";
import {TabularComponent} from "./tabular/tabular.component";

@NgModule({
  imports: [
    BrowserModule,
    HighlightJsModule,
    AppRoutingModule,
    FormsModule,
    Ng2PageScrollModule.forRoot(),
    Ng2SynModule.forRoot()
  ],
  providers:[
    HighlightJsService,
    AppService
  ],
  declarations: [
    AppComponent,
    NavComponent,
    DropdownComponent,
    BreadcrumbComponent,
    TabsComponent,
    TooltipComponent,
    ModalComponent,
    MyCustomModalComponent,
    TabularComponent
  ],
  bootstrap:    [
      AppComponent
  ],
  entryComponents:[MyCustomModalComponent]
})
export class AppModule { }
