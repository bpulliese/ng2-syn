import {Component, ViewChild, ElementRef, ChangeDetectionStrategy} from '@angular/core';
import {INav,INavItem,NavDirection,NavTheme} from "../../modules/nav/nav.interfaces";
import {AppService} from "../app.service";
import {BaseComponent} from "../base.component";

@Component({
    selector: 'tabs-component',
    templateUrl: 'tabs.tpl.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styles: [':host { display:flex; flex: 1; min-width: 0; }']
})
export class TabsComponent extends BaseComponent {

    public tabs:any[] = [
        {title: 'Dynamic Title 1', content: 'Dynamic content 1'},
        {title: 'Dynamic Title 2', content: 'Dynamic content 2', disabled: true},
        {title: 'Dynamic Title 3', content: 'Dynamic content 3', removable: true},
        {title: 'Dynamic Title 4', content: 'Dynamic content 4', customClass: 'customClass'}
    ];

    public alertMe():void {
        setTimeout(function ():void {
            alert('You\'ve selected the alert tab!');
        });
    }

    public setActiveTab(index:number):void {
        this.tabs[index].active = true;
    }

    public removeTabHandler(/*tab:any*/):void {
        console.log('Remove Tab handler');
    }

    private contentsNav:INav = {
        config:{
            direction:NavDirection.Vertical,
            theme:NavTheme.Plain
        },
        items: [
            {
                label: 'Contents'
            },
            {
                label: "Usage",
                path:'#usage'
            },
            {
                label: "Examples",
                path:'#examples'
            },
            {
                label: "Code",
                path:'#code'
            },
            {
                label: "API Reference",
                path:'#api'
            }
        ]
    };

    constructor(appService:AppService){
        super(appService);
    }
}
