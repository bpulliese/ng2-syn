import {Component} from '@angular/core';
import {INav,INavItem,NavDirection,NavTheme} from "../../modules/nav/nav.interfaces";
import {AppService} from "../app.service";
import {BaseComponent} from "../base.component";
import {ITabularColumn, TabularColumnTypes} from "../../modules/tabular/tabular-column.interface";
import {TabularColumn} from "../../modules/tabular/tabular-column.model";
import {ITabularConfig,TabularSize} from "../../modules/tabular/tabular-config.interface";
import {ActionConfigRouteType} from "../../modules/tabular/actions-config.interface";

@Component({
    selector: 'tabular-component',
    templateUrl: 'tabular.tpl.html',
    styles: [':host { display:flex; flex: 1; min-width: 0; }']
})
export class TabularComponent extends BaseComponent {


    private searchTerm:string = "";
    private rowData:any[] = [];
    private columnData:TabularColumn[] = [
        new TabularColumn('avatar','Avatar', TabularColumnTypes.Icon, false, 'tabular__avatar text-gray-light text-center'),
        new TabularColumn('name','Name', TabularColumnTypes.String, true),
        new TabularColumn('email','Email', TabularColumnTypes.String, true),
        new TabularColumn('created','Created', TabularColumnTypes.DateTime, true),
        new TabularColumn('status','Status', TabularColumnTypes.Status, true),
        new TabularColumn('actions','Actions', TabularColumnTypes.Actions, false, 'tabular__actions')
    ];
    private tabularConfig:ITabularConfig = {
        size: TabularSize.Default,
        pagination:{
            id: 'DEMO_TABULAR_INSTANCE',
            itemsPerPage: 5,
            currentPage: 1
        },
    };


    /**
     * Function used in the callback actions
     * @param type
     * @param data
     */
    onActionClickHandler = (type,data) => {
      alert('You clicked the '+type+' button. Arguments:'+type+ ' and '+data);
    };


    /**
     * Refresh data handler for data grid.
     */
    private refreshDataHandler(){
        this.rowData = [];
        this.getTabularData();
    }

    private getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }

    /**
     * Static data for example
     */
    private getTabularData(){
        for(let i=0;i<20;i++)
            this.rowData.push({
                avatar:'account_circle',
                name:"Mr Brian Pulliese",
                email:"brian@synapze.com.au",
                created:Date.now(),
                status:(this.getRandomArbitrary(1,5)<3),
                actions:[
                    {
                        id    : "row_person_view",
                        label : '<i class="material-icons">visibility</i>',
                        route : [ '/tabular' ],
                        routeType: ActionConfigRouteType.Default,
                        disabledConfig:{disabled:true,tooltip:"Not enough permission"}
                    },
                    {
                        id    : "row_person_edit",
                        label : '<i class="material-icons">edit</i>',
                        routeType: ActionConfigRouteType.Callback,
                        callback:[this.onActionClickHandler,"edit",1]
                    },
                    {
                        id    : "row_person_delete",
                        label : '<i class="material-icons">delete</i>',
                        routeType: ActionConfigRouteType.Callback,
                        callback:[this.onActionClickHandler,"delete",1]
                    },
                    {
                        id    : "row_person_delete_forever",
                        label : '<i class="material-icons">delete_forever</i>',
                        routeType: ActionConfigRouteType.Callback,
                        callback:[this.onActionClickHandler,"perm-delete",1]
                    }
                ]
            });
    }



    private contentsNav:INav = {
        config:{
            direction:NavDirection.Vertical,
            theme:NavTheme.Plain
        },
        items: [
            {
                label: 'Contents'
            },
            {
                label: "Usage",
                path:'#usage'
            },
            {
                label: "Examples",
                path:'#examples'
            },
            {
                label: "Code",
                path:'#code'
            },
            {
                label: "API Reference",
                path:'#api'
            }
        ]
    };

    constructor(appService:AppService){
        super(appService);
        this.getTabularData();
    }
}
