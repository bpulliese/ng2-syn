import {Modal} from "../../modules/modal/modal.annotation";
import {Component} from "@angular/core";

@Component({
    selector: "my-custom-modal",
    template: `
    <div class="syn-modal in fade">
        <div class="syn-modal-dialog" role="document">
            <div class="syn-modal-content">
                <div class="syn-modal-header">
                    <h2 class="syn-modal-title">Modal (todo)</h2>
                </div>
                <div class="syn-modal-body">
                   <ul>
                      <li *ngFor="let item of listItems">{{item}}</li>
                    </ul>
                </div>
                <div class="syn-modal-footer">
                    <button type="button" class="syn-btn syn-btn-subtle" (click)="onCancel()">Close</button>
                    <button type="button" class="syn-btn syn-btn-blue" (click)="onOk()">Save changes</button>
                </div>
            </div>
        </div>
     </div>
`
})
@Modal()
export class MyCustomModalComponent {
    protected onSuccess: Function;
    protected destroy: Function;
    protected close: Function;
    public listItems:string[] = ["list item 1", "list item 2", "list item 3"];

    onCancel(): void{
        this.close();
        this.destroy();
    }

    onOk(): void{
        this.close();
        this.destroy();
        this.onSuccess(this.listItems);
    }
}