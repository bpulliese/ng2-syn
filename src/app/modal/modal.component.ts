import {Component, ComponentRef} from '@angular/core';
import {INav,INavItem,NavDirection,NavTheme} from "../../modules/nav/nav.interfaces";
import {AppService} from "../app.service";
import {BaseComponent} from "../base.component";
import {ModalService} from "../../modules/modal/modal.service";
import {AppModule} from "../app.module";
import {MyCustomModalComponent} from "./custom-modal.component";

@Component({
    selector: 'modal-component',
    templateUrl: 'modal.tpl.html',
    styles: [':host { display:flex; flex: 1; min-width: 0; }']
})
export class ModalComponent extends BaseComponent {

    private contentsNav:INav = {
        config:{
            direction:NavDirection.Vertical,
            theme:NavTheme.Plain
        },
        items: [
            {
                label: 'Contents'
            },
            {
                label: "Usage",
                path:'#usage'
            },
            {
                label: "Examples",
                path:'#examples'
            },
            {
                label: "Code",
                path:'#code'
            },
            {
                label: "API Reference",
                path:'#api'
            }
        ]
    };

    private modalRef:ComponentRef<MyCustomModalComponent>;
    openModal(): void{
        this.modalRef = this.modalService.create<MyCustomModalComponent>(MyCustomModalComponent, {
            onSuccess: (listItems) => {
                alert(listItems.join(', '));
            }
        });
    }



    constructor(appService:AppService, private modalService:ModalService) {
        super(appService);
    }
}