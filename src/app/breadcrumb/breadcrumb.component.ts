import {Component, ViewChild, ElementRef} from '@angular/core';
import {INav,INavItem,NavDirection,NavTheme} from "../../modules/nav/nav.interfaces";
import {AppService} from "../app.service";
import {BaseComponent} from "../base.component";

@Component({
    selector: 'breadcrumb-component',
    templateUrl: 'breadcrumb.tpl.html',
    styles: [':host { display:flex; flex: 1; min-width: 0; }']
})
export class BreadcrumbComponent extends BaseComponent {

    private contentsNav:INav = {
        config:{
            direction:NavDirection.Vertical,
            theme:NavTheme.Plain
        },
        items: [
            {
            label: 'Contents'
            },
            {
                label: "Usage",
                path:'#usage'
            },
            {
                label: "Examples",
                path:'#examples'
            },
            {
                label: "Code",
                path:'#code'
            },
            {
                label: "Service",
                path:'#service'
            },
            {
                label: "API Reference",
                path:'#api'
            }
        ]
    };

    constructor(appService:AppService){
        super(appService);
    }
}
