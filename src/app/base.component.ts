import {Component, ViewChild, ElementRef} from '@angular/core';
import {AppService} from "./app.service";
import {INavItem} from "../modules/nav/nav.interfaces";


export class BaseComponent  {
    @ViewChild('container')
    private container: ElementRef;


    private scrollTo = (item:INavItem) =>{
        this.appService.scrollTo(item.path, this.container.nativeElement);
    };

    private gotoRoute = (item:INavItem) =>{
        this.appService.gotoRoute(item.path);
    };

    constructor(private appService:AppService){
    }
}
