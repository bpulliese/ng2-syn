import {NgModule, ModuleWithProviders} from '@angular/core';
import {NavModule } from './nav/nav.module';
import {TabsModule} from "./tabs/tabs.module";
import {DropdownModule} from "./dropdown/dropdown.module";
import {BreadcrumbModule} from "./breadcrumb/breadcrumb.module";
import {TooltipModule} from "./tooltip/tooltip.module";
import {NavConfig} from "./nav/nav.config";
import {BreadcrumbConfig} from "./breadcrumb/breadcrumb.config";
import {DropdownConfig} from "./dropdown/dropdown.config";
import {TabsetConfig} from "./tabs/tabset.config";
import {TooltipConfig} from "./tooltip/tooltip.config";
import {ModalService} from "./modal/modal.service";
import {BreadcrumbService} from "./breadcrumb/breadcrumb.service";
import {DropdownService} from "./dropdown/dropdown.service";
import {TabularModule} from "./tabular/tabular.module";
import {ModalModule} from "./modal/modal.module";
import {TabularOrderByService} from "./tabular/tabular-order-by.service";
import {TabularConfig} from "./tabular/tabular.config";


@NgModule({
    exports: [
        NavModule,
        TabsModule,
        DropdownModule,
        BreadcrumbModule,
        TooltipModule,
        TabularModule,
        ModalModule,
        TabularModule
    ]
})
export class Ng2SynModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: Ng2SynModule,
            providers:[
                NavConfig,
                BreadcrumbConfig,
                BreadcrumbService,
                DropdownConfig,
                TabsetConfig,
                TooltipConfig,
                ModalService,
                DropdownService,
                TabularOrderByService,
                TabularConfig
            ]
        };
    }
}

export * from './nav';
export * from  './tabs';
export * from  './dropdown';
export * from  './breadcrumb';
export * from './tooltip';
export * from './modal';
export * from './tabular';
