import {AbstractControl} from "@angular/forms";

export class SynapzeValidators {

    /**
     * Confirm password validator
     * Form must have a field called "password".
     * Would be nice to make this part more generic.
     * @param {Control} c
     * @returns {{noMatch: boolean}}
     */
    static confirmPassword(c:AbstractControl):{ [s: string]: boolean } {
        return (c.root['controls'] && c.value !== c.root['controls'].password.value)? {noMatch:true} : null;
    }


}


