export enum NavDirection{
    Horizontal,
    Vertical
}

export enum NavTheme{
    Blue,
    Plain,
    Default
}

export interface INavConfig{
    direction:NavDirection;
    theme?:NavTheme;
}

export interface INavItem{
    label?:string;
    path?:any;
    icon?:string;
    spacer?:boolean;
    active?:boolean; //TODO - Change current active implementation to use this active property instead.
    items?:INavItem[]; //TODO - support child items.
}

export interface INav{
    config:INavConfig
    items:INavItem[];
}
