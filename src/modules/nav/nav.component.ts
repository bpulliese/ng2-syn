import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { NavConfig } from './nav.config';
import  {INav, INavItem, NavDirection, NavTheme} from './nav.interfaces';
import {forEach} from "@angular/router/src/utils/collection";

@Component({
    selector: 'syn-nav',
    template: `<ul class="syn-nav" [ngClass]="classMap">
                    <li *ngFor='let item of items' [ngClass]="{'syn-nav-item': isLink(item), 'syn-nav-heading': isHeading(item), 'syn-nav-spacer': isSpacer(item)}">
                        <!-- group title -->
                        <h5 *ngIf="isHeading(item)">{{item.label}}</h5>
                        <!-- link item -->
                        <a *ngIf="isLink(item)"  class="syn-nav-link" [ngClass]="{'active': isActive(item)}" href="#" (click)="executeCallback($event,item)"><i *ngIf="!!(item.icon)" class="material-icons">{{item.icon}}</i>  <span>{{item.label}}</span></a>
                        <!-- spacer -->
                        <hr *ngIf="isSpacer(item)">
                    </li>
                </ul>`
})
export class NavComponent implements NavConfig, OnInit, OnDestroy{

    /** data object : contains config, nav items etc.. **/
    @Input()
    public get data():INav{
        return this._data;
    }
    public set data(nav:INav){
        this._data = nav;
    }

    /** The function to call when a nav item is clicked **/
    @Input()
    public get callback():Function{
        return this._callback;
    }
    public set callback(Fn:Function){
        this._callback = Fn;
    }


    protected classMap:any = {};
    protected _data:INav;
    protected _callback:Function;

    public items: INavItem[];
    public direction:NavDirection;
    public theme:NavTheme;

    public constructor(private config: NavConfig) {
        Object.assign(this, config);
    }

    public ngOnInit():any {
        this.setData();
        this.setClassMap();
    }

    public ngOnChanges():any {
        this.setData();
        this.setClassMap();
    }

    public ngOnDestroy():any {

    }

    private setData(){
        if(this.data){
            this.items = this.data.items;
            this.direction = this.data.config.direction;
            if(this.data.config.theme)
                this.theme = this.data.config.theme;
        }
    }

    private executeCallback(event:Event,item:INavItem){
        this.deactivateItems();
        item.active=true;//activate selected
        this.callback(item);
        //event.stopPropagation();
        //event.stopImmediatePropagation();
        return false;
    }

    private deactivateItems(){
        let activeItem = this.items.filter(item => {
            return (item.active);
        });
        if(activeItem.length)
            for(let i=0;i<activeItem.length;i++)
                activeItem[i].active = false; //deactivate item
    }

    private isVertical(){
        if(this.data)
            return (this.direction==NavDirection.Vertical);
        else
            return (this.direction==NavDirection.Vertical);
    }

    private isHeading(item:INavItem):boolean{
        return !(item.path) && !(item.spacer);
    }

    private isSpacer(item:INavItem):boolean{
        return !!(item.spacer);
    }

    private isLink(item:INavItem):boolean{
        return !!(item.path);
    }

    private isActive(item:INavItem):boolean{
        return !!(item.active) && item.active;
    }

    protected setClassMap():void {
        this.classMap = {
            'syn-nav-vertical': this.isVertical(),
            'syn-nav-blue': (this.theme==NavTheme.Blue),
            'syn-nav-plain': (this.theme==NavTheme.Plain),
        };
    }


}