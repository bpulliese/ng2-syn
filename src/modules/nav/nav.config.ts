import { Injectable } from '@angular/core';
import {NavDirection, NavTheme, INavConfig, INavItem} from "./nav.interfaces";

/**
 * Configuration service, provides default values for the NavComponent.
 */
@Injectable()
export class NavConfig implements INavConfig{

    //@config - default nav items
    public items: INavItem[] = [];

    //@config - Whether the nav should be rendered horizontally or vertically
    public direction: NavDirection  = NavDirection.Horizontal;

    //@config - default nav component theme
    public theme:NavTheme = NavTheme.Default;

}