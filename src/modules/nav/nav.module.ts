import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import {RouterModule} from "@angular/router";

import { NavComponent } from './nav.component';
import { NavConfig } from './nav.config';

@NgModule({
    imports: [CommonModule,RouterModule],
    declarations: [NavComponent],
    exports: [NavComponent]
})
export class NavModule {
    public static forRoot(): ModuleWithProviders {return {ngModule: NavModule, providers: [NavConfig]};}
}