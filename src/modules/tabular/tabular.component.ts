import {Component,Input, Output, EventEmitter, Type} from '@angular/core';
import {TabularColumn} from './tabular';
import {ITabularConfig, TabularSize} from "./tabular-config.interface";
import {IActionsConfig} from "./actions-config.interface";
import {TabularOrderByService, OrderByDirection} from "./tabular-order-by.service";
import {TabularConfig} from "./tabular.config";


@Component({
  selector: 'syn-tabular',
  templateUrl: './tabular.tpl.html',
  styles:['.tabular__sorter{position:relative;cursor:pointer} th .material-icons{position: absolute;font-size: 18px;}']
})


export class TabularComponent {

  /**
   * Collection of column models
   */
  @Input() columns:Array<TabularColumn>;

  /**
   * Collection of data rows
   * @note this is not strict enough. How can we give it a type when it's dynamic?.
   */
   @Input() rows:Array<any>;


  /**
   * Tabular configuration
   * IPaginationInstance, ISearchConfig
   */
  @Input()
  public get config():ITabularConfig  {
    return this._config;
  }
  public set config(c:ITabularConfig)  {
    this._config = c;
  }

  /** The function to call when a action item is clicked **/
  @Input()
  public get callback():Function{
    return this._callback;
  }
  public set callback(Fn:Function){
    this._callback = Fn;
  }

  /**
   * Search term is used in the simple search pipe
   * Array of objects: *ngFor="#row of rows | simpleSearch : 'the search term'"
   */
  @Input()
  public get searchTerm():string{
    return this._searchTerm;
  }
  public set searchTerm(term:string){
    this._searchTerm = term;
  }

  /**
   * Event fired when refresh is called.
   * Host should refresh data of input.
   * @type {EventEmitter<any>}
   */
  @Output() refresh: EventEmitter<boolean> = new EventEmitter<boolean>();


  /**
   * Order by used by orderBy service
   * @example *ngFor="#person of people | orderBy : ['-age', 'firstName']"
   * @example *ngFor="#person of people | orderBy : ['+age', 'firstName']"
   */
  public orderBy:Array<string> = this.defaultOrderBy;


  private defaultOrderBy:Array<string> = ['id'];
  private defaultOrderByDirection:OrderByDirection;
  protected _callback:Function;
  protected _config:ITabularConfig;
  protected _searchTerm:string;


  public constructor(private conf: TabularConfig, private orderByService:TabularOrderByService) {
    Object.assign(this, conf);
  }



  /**
   * ngOnInit is called right after the directive's data-bound properties have been
   * checked for the first time, and before and of its children have been checked.
   * It is invoked only once when the directive is instantiated.
   */
  ngOnInit(){

  }

  private get iconDirection():string{
    return (this.defaultOrderByDirection===OrderByDirection.Ascending)? "keyboard_arrow_up" : "keyboard_arrow_down";
  }

  /**
   * Calls the parsed callback with optional arguments
   * @param event
   * @param cb
   * @returns {boolean}
   */
  private executeCallback(event:Event,cb:any[]){
    if(cb.length){
      if(cb.length==1){ //if callback has no arguments
        cb[0]();
      }
      else{ //if callback has 1 or more arguments
        let args:any[] = [];
        for(let i=1;i<cb.length;i++)
          args.push(cb[i]);
        cb[0].apply(this,args);
      }
    }
    return false;
  }


  /**
   * Get the action tooltip if it exists
   * @param action
   * @returns {string}
     */
  private getActionTooltip(action:IActionsConfig):string{
    return (action && action.disabledConfig)? action.disabledConfig.tooltip : "";
  }


  private getActionDisabledState(action:IActionsConfig):boolean{
    return (action && action.disabledConfig)? action.disabledConfig.disabled : false;
  }


  /**
   * Handles the column header click event.
   * @param key
   */
  private onSortClickHandler(key:string){
    this.orderBy = ([key]===this.orderBy)? this.defaultOrderBy : [key];
    this.orderByData();
  }



  /**
   * Order collection via full collection and not via pipe.
   * The pagination pipe will only return the paginated amount.
   * Which means the order by filter will only be applied to whats paginated
   * and not the full collection.
   */
  private orderByData(){
    let direction:string;
    if(this.defaultOrderByDirection===OrderByDirection.Ascending){
      direction = '-';
      this.defaultOrderByDirection = OrderByDirection.Descending;
    } else {
      direction = '+';
      this.defaultOrderByDirection = OrderByDirection.Ascending;
    }

    this.orderByService.doTransform(this.rows, [direction+this.orderBy[0]]);
  }


  /**
   * Helper to determine if tabular instance is in small mode
   * @returns {boolean}
   */
  private isSmall():boolean{
    return (this.config.size===TabularSize.Small);
  }

}
