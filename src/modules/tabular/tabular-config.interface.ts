import {PaginationInstance} from "ng2-pagination";

export enum TabularSize {
  Default,
  Small,
  Large
}

export interface ITabularConfig{
  size:TabularSize;
  pagination:PaginationInstance,
}
