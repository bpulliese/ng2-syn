export {ITabularConfig, TabularSize} from "./tabular-config.interface";
export {ITabularColumn,TabularColumnTypes} from "./tabular-column.interface";
export {TabularColumn} from "./tabular-column.model";
export {TabularComponent} from "./tabular.component";
export {ActionConfigRouteType, IActionsConfig} from "./actions-config.interface";
export {TabularPaginationComponent} from "./tabular-pagination.component";





