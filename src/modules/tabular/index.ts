export * from './tabular.component';
export * from './tabular.module';
export * from './actions-config.interface';
export * from './tabular-column.interface';
export * from './tabular-column.model';
export * from './tabular-config.interface';
export * from './tabular.config';
export * from './tabular-order-by.service';
