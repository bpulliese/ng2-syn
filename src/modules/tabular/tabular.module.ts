import {NgModule, ModuleWithProviders} from '@angular/core';
import {TabularComponent} from "./tabular.component";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {Ng2PaginationModule} from "ng2-pagination";
import {TabularPaginationComponent} from "./tabular-pagination.component";
import {TabularOrderByService} from "./tabular-order-by.service";
import {TabularConfig} from "./tabular.config";
import {TooltipModule} from "../tooltip/tooltip.module";
import {SimpleSearchPipe} from "../utilities/pipes/simple-search.pipe";

@NgModule({
  declarations: [
    TabularComponent,
    TabularPaginationComponent,
    SimpleSearchPipe
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpModule,
    RouterModule,
    Ng2PaginationModule,
    TooltipModule
  ],
  providers:[
    TabularOrderByService,
    TabularConfig
  ],
  exports:[
    TabularComponent,
    TabularPaginationComponent
  ]
})

export class TabularModule {
  public static forRoot(): ModuleWithProviders {return {ngModule: TabularModule, providers: [TabularOrderByService,TabularConfig]};}
}


