import { Injectable } from '@angular/core';
import {ITabularConfig, TabularSize} from "./tabular-config.interface";
import {OrderByDirection} from "./tabular-order-by.service";


/**
 * Configuration service, provides default values for the NavComponent.
 */
@Injectable()
export class TabularConfig {

    /**
     * Tabular configuration
     * IPaginationInstance, ISearchConfig
     */
    public config:ITabularConfig = {
        size:TabularSize.Default,
        pagination:{
            id: 'DEFAULT_TABULAR_INSTANCE',
            itemsPerPage: 5,
            currentPage: 1
        },
    };

    /**
     * Default order by value
     * @type {string[]}
     */
    public defaultOrderBy:Array<string> = ['id'];

    /**
     * Default order by direction
     * @type OrderByDirection
     */
    public defaultOrderByDirection:OrderByDirection = OrderByDirection.Ascending;


}