import {Component, Input, Output, EventEmitter, ChangeDetectionStrategy, ViewEncapsulation} from '@angular/core'

@Component({
    selector: 'syn-tabular-pagination',
    template: `<pagination-template  #p="paginationApi"
                         [id]="id"
                         [maxSize]="maxSize"
                         (pageChange)="pageChange.emit($event)">
    <ul class="syn-pagination" 
        role="navigation" 
        *ngIf="!(autoHide && p.pages.length <= 1)">
        <li class="syn-pagination-previous" [class.disabled]="p.isFirstPage()" *ngIf="directionLinks"> 
            <a *ngIf="1 < p.getCurrent()" (click)="p.previous()">
                <i class="material-icons">chevron_left</i> {{ previousLabel }}
            </a>
            <span *ngIf="p.isFirstPage()">
                <i class="material-icons">chevron_left</i> {{ previousLabel }} 
            </span>
        </li>
        <li *ngFor="let page of p.pages">
            <a [class.current]="p.getCurrent() === page.value" (click)="p.setCurrent(page.value)">
                {{ page.label }}
            </a>
        </li>
        <li class="syn-pagination-next" [class.disabled]="p.isLastPage()" *ngIf="directionLinks">
            <a *ngIf="!p.isLastPage()" (click)="p.next()" >
                 {{ nextLabel }} <i class="material-icons">chevron_right</i>
            </a>
            <span *ngIf="p.isLastPage()">
                 {{ nextLabel }} <i class="material-icons">chevron_right</i>
            </span>
        </li>
    </ul>
    </pagination-template>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class TabularPaginationComponent {

    @Input() id: string;
    @Input() maxSize: number = 7;
    @Input()
    get directionLinks(): boolean {
        return this._directionLinks;
    }
    set directionLinks(value: boolean) {
        this._directionLinks = !!value && <any>value !== 'false';
    }
    @Input()
    get autoHide(): boolean {
        return this._autoHide;
    }
    set autoHide(value: boolean) {
        this._autoHide = !!value && <any>value !== 'false';
    }
    @Input() previousLabel: string = 'Previous';
    @Input() nextLabel: string = 'Next';
    @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();

    private _directionLinks: boolean = true;
    private _autoHide: boolean = false;
}