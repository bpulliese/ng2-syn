import { Injectable } from '@angular/core';
import { IBreadcrumbConfig } from './breadcrumb.interfaces';


/**
 * Configuration service, provides default values for the BreadcrumbComponent.
 */
@Injectable()
export class BreadcrumbConfig implements IBreadcrumbConfig{
    
    //@config - default prefix which is the first breadcrumb which routes to the base root when clicked.
    public prefix: string = 'Dashboard';

}