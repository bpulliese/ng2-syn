import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { BreadcrumbComponent } from './breadcrumb.component';
import { BreadcrumbService } from './breadcrumb.service';
import { BreadcrumbConfig } from './breadcrumb.config';


@NgModule({
    imports: [CommonModule ],
    declarations: [BreadcrumbComponent],
    exports: [BreadcrumbComponent]
})

export class BreadcrumbModule {
    public static forRoot(): ModuleWithProviders {return {ngModule: BreadcrumbModule, providers: [BreadcrumbConfig, BreadcrumbService]};}
}