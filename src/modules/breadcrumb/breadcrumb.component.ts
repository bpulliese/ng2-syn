import {Component, Input, OnInit, OnChanges} from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';
import {BreadcrumbService} from './breadcrumb.service';
import { BreadcrumbConfig } from './breadcrumb.config';

/**
 * This component shows a breadcrumb trail for available routes the router can navigate to.
 * It subscribes to the router in order to update the breadcrumb trail as you navigate to a component.
 */
@Component({
    selector: 'syn-breadcrumb',
    template: `
        <ul class="syn-breadcrumb">
            <li *ngFor="let url of _urls; let last = last" [ngClass]="{'active': last}" class="syn-breadcrumb-item"> <!-- disable link of last item -->
                <a role="button" class="syn-breadcrumb-link" *ngIf="!last && url == prefix" (click)="navigateTo('/')">{{url}}</a>
                <a role="button" class="syn-breadcrumb-link" *ngIf="!last && url != prefix" (click)="navigateTo(url)">{{friendlyName(url)}}</a>
                <span *ngIf="last">{{friendlyName(url)}}</span>
                <span *ngIf="last && url == prefix">{{friendlyName('/')}}</span>
            </li>
        </ul>
    `
})
export class BreadcrumbComponent implements BreadcrumbConfig, OnInit, OnChanges {
    @Input() prefix:string;
    
    private _urls: string[];
    private _routerSubscription: any;

    constructor(
        private router: Router,
        private breadcrumbService: BreadcrumbService,
        private config: BreadcrumbConfig
    ) {
         Object.assign(this, config);
    }

    ngOnInit(): void {
        this._urls = new Array();
        
        if (this.prefix.length > 0) {
            this._urls.unshift(this.prefix);
        }

        this._routerSubscription = this.router.events.subscribe((navigationEnd:NavigationEnd) => {
            this._urls.length = 0; //Fastest way to clear out array
            this.generateBreadcrumbTrail(navigationEnd.urlAfterRedirects ? navigationEnd.urlAfterRedirects : navigationEnd.url);
        });
    }

    ngOnChanges(): void {
        if (!this._urls) {
            return;
        }
        
        this._urls.length = 0;
        this.generateBreadcrumbTrail(this.router.url);
    }

    generateBreadcrumbTrail(url: string): void {
        if (!this.breadcrumbService.isRouteHidden(url)) {
            //Add url to beginning of array (since the url is being recursively broken down from full url to its parent)
            this._urls.unshift(url);
        }

        if (url.lastIndexOf('/') > 0) {
            this.generateBreadcrumbTrail(url.substr(0, url.lastIndexOf('/'))); //Find last '/' and add everything before it as a parent route
        } else if (this.prefix.length > 0) {
            this._urls.unshift(this.prefix);
        }
    }

    navigateTo(url: string): void {
        this.router.navigateByUrl(url);
    }

    friendlyName(url: string): string {
        return !url ? '' : this.breadcrumbService.getFriendlyNameForRoute(url);
    }

    ngOnDestroy(): void {
        this._routerSubscription.unsubscribe();
    }

}
