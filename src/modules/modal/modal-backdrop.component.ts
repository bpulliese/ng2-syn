
import {Component} from "@angular/core";
import {Modal} from "./modal.annotation";

@Component({
    selector: "syn-modal-backdrop",
    template: `<div class="syn-modal-backdrop fade in" (click)="dismiss()"></div>`
})
@Modal()
export class ModalBackdropComponent
{
    protected destroy: Function;
    protected close: Function;

    dismiss(): void{
        this.close();
        this.destroy();
    }
}